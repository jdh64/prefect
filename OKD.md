### SOM Clinical Notes Explorer
# Instructions for OKD

## Duke Openshift User Group
https://openshift-docs.cloud.duke.edu/user-guide/dukeShibboleth/#creating-the-container-images

## Logging into OKD environment
Dev: https://console.apps.dev.okd4.fitz.cloud.duke.edu/

### Use `oc` command line tool
Go to the above and select "Command line tools" from the [?] help menu.

### Log in with the `oc` tool
Go to the above and select "Copy login command" from the upper-right drop-down menu.

### CICD Integration
1. create a Service Account in OKD
```
oc get sa
oc create sa <name>
oc describe sa <name>
oc policy add-role-to-user admin -z <name>
```
2. In the OKD web console, get the "token" value from the new `service-account-token` you created.  In Gitlab, create a new Variable in Settings > CI/CD > Variables called `OKD_DEVELOP_TOKEN`, or whatever is in your `.gitlab-ci.yml`.  Set the "Value" to the token value.  Check the "Mask variable" option and uncheck the "Expand variable reference" option.  The Type should be "Variable".

### Installing configuration with `oc`
```
# general environment
oc create configmap jdh64-prefect-env --from-env-file=.env.okd.dev
oc create secret generic jdh64-prefect-env --from-env-file=.env.okd.dev.secrets

# server and api configuration
oc create -f okd/persistentVolumeClaimPostgres.yml
oc create -f okd/dev/deploymentConfigPostgres.yml
oc create -f okd/dev/deploymentConfigServer.yml
oc create -f okd/dev/deploymentConfigAgent.yml
oc create -f okd/servicePostgres.yml
oc create -f okd/serviceServer.yml
oc create -f okd/dev/routeApp.yml

# register a deployment
oc exec --stdin --tty dc/jdh64-prefect-agent -- /bin/bash
    # python flow.py
    python deployment.py

# -- or -- 

oc create -f okd/dev/deploymentConfigRegister.yml
# wait
oc delete -f okd/dev/deploymentConfigRegister.yml

```

### Post-install configuration (you need a hostname!)
`.env.okd.dev.secrets` should have:
```
POSTGRES_USER=
POSTGRES_PASSWORD=
PREFECT_API_DATABASE_CONNECTION_URL=postgresql+asyncpg://<USER>:<PASS>@<HOSTNAME>:5432/prefect
```