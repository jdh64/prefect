from prefect.deployments import Deployment
from prefect.server.schemas.schedules import CronSchedule
from flow import say_hello_say_goodbye

deployment = Deployment.build_from_flow(
    flow=say_hello_say_goodbye,
    name="jdh64-deployment", 
    version=1.1,
    schedule=(CronSchedule(
        # every 57 minutes
        cron="*/57 * * * *", 
        timezone="America/New_York"
    )),
    work_queue_name="jdh64-work-queue",
    work_pool_name="default-agent-pool",
)
deployment.apply()