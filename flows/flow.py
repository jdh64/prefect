from prefect import flow, task
import numpy as np

@task
def say_hello(name):
    print(f"hello {name}")

@task
def say_goodbye(name):
    print(f"goodbye {name}")

@flow(name="say_hello_say_goodbye")
def say_hello_say_goodbye(names=["arthur", "trillian", "ford", "marvin"]):
    name = np.random.choice(names)
    say_hello(name)
    say_goodbye(name)

if __name__ == "__main__":
    say_hello_say_goodbye(["arthur", "trillian", "ford", "marvin"])
